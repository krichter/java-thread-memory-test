package de.richtercloud.java.thread.memory.test;

import java.util.LinkedList;
import java.util.Queue;
import static java.util.concurrent.TimeUnit.SECONDS;
import org.junit.Test;

public class TheTest {

    @Test
    public void main() throws InterruptedException {
        Queue<Thread> queue = new LinkedList<>();
        int count = 10000;
        for(int i=0; i<count; i++) {
            Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(SECONDS.toMillis(10));
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            });
            thread.start();
            queue.add(thread);
        }
        while(!queue.isEmpty()) {
            Thread head = queue.poll();
            head.join();
        }
    }
}
